({
	callServerAction : function(component, event) {
		var serverActionName = event.getParam('arguments').serverActionName;
		var clientParams = event.getParam('arguments').clientParams;
		var serverParams = event.getParam('arguments').serverParams;
		var action = component.get("c."+serverActionName);

		action.setParams(serverParams);
		action.setCallback(this, function(response) {
			if (response.getState() === "SUCCESS") {
				if(clientParams.onSuccess) {
					clientParams.onSuccess(response); //trigger function onSuccess()
				}
				if(clientParams.eventOnSuccess) {
					var eventOnSuccess = component.getEvent(clientParams.eventOnSuccess.name);
					eventOnSuccess.setParams(clientParams.eventOnSuccess.params);   
					if(eventOnSuccess) {
						eventOnSuccess.fire();
					}
				}
			} else if (response.getState() === "ERROR") {
				var errors = response.getError();
				if (errors) { //by default, log the error with the message
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + errors[0].message);

						/*$A.get("e.c:evtShowToast").setParams({
							type:"error",
							title:"Error with back end operation",
							message:errors[0].message                          
						}).fire();*/
					}
				} else {
					console.log("Unknown error");
				}

				if(clientParams.onError) {
					clientParams.onError(response); //trigger function onError()
				}
			}
		});
		action.setCallback(this, function(response) {
			console.log(serverActionName + " ABORTED");
			if(clientParams.onAbort) {
				clientParams.onAbort(response);
			}
		}, "ABORTED");

		if(clientParams.abortable) {
			action.setAbortable();
		}

		if(clientParams.storable) {
			var ignoreExisting = false;
			if(clientParams.ignoreExisting) {
				ignoreExisting = clientParams.ignoreExisting;
			}

			if(String(ignoreExisting) == "true") {
				action.setStorable({"ignoreExisting" : ignoreExisting});
			} else {
				action.setStorable();
			}
		}

		$A.enqueueAction(action);
	}
})