({
	doInit : function(component, event) {
		var tcProducts = [
			{
                'name' : 'Rollstock',
                'requiredFieldSet' : 'RollstockRequiredFieldSetName',
                'optionalFieldSet' : 'RollstockOptionalFieldSetName'
            },
			{
                'name' : 'Pouch - Box Pouch',
                'requiredFieldSet' : 'BoxPouchRequiredFieldSetName',
                'optionalFieldSet' : 'BoxPouchOptionalFieldSetName',
                'thumbnail' : 'https://tcpackagingtemp--c.na59.visual.force.com/resource/1506007277000/BoxPouch'
            },
			{
                'name' : 'Pouch - Box Pouch with Terminated Side Gusset',
                'requiredFieldSet' : 'BoxPouchTerminatedSideGussetRequiredFieldSetName',
                'optionalFieldSet' : 'BoxPouchTerminatedSideGussetOptionalFieldSetName',
                'thumbnail' : 'https://tcpackagingtemp--c.na59.visual.force.com/resource/1506007348000/TermBoxPouch'
            },
			{
                'name' : 'Pouch - Fin Seal',
                'requiredFieldSet' : 'FinSealRequiredFieldSetName',
                'optionalFieldSet' : 'FinSealOptionalFieldSetName'
            }
		]

        var tcProductsFieldSets = [
            {
                'name': 'RollstockRequiredFieldSetName',
                'fields': [
                    {
                        'name': 'webWidth',
                        'type': 'numeric-with-unit',
                        'label': 'Finished Web Width',
                        'units': ['inches', 'mm']
                    },
                    {
                        'name': 'nbImpressionsWeb',
                        'type': 'numeric',
                        'label': '# of Impressions Across Web'
                    },
                    {
                        'name': 'coreSize',
                        'type': 'choice-single',
                        'label': 'Core Size',
                        'options': ['3"', '6"', 'Unknown', 'N/A']
                    },
                    {
                        'name': 'cutoffRepeat',
                        'type': 'numeric-with-unit',
                        'label': 'Cutoff/Repeat',
                        'units': ['inches', 'mm']
                    },
                    {
                        'name': 'nbImpressionsCutoff',
                        'type': 'numeric',
                        'label': '# of Impressions per Cutoff'
                    }
                ]
            },
            {
                'name': 'RollstockOptionalFieldSetName',
                'fields': [
                    {
                        'name': 'finishingRequirements',
                        'type': 'choice-multiple',
                        'label': 'Finishing Requirements',
                        'options': ['Laser Score', 'Other']
                    },
                    {
                        'name': 'quantityUomRoll',
                        'type': 'numeric-with-unit',
                        'label': 'Quantity & UOM/Roll',
                        'units': ['Imps', 'M Imps', 'Kg', 'Lbs', 'Linear Feet', 'Linear Meters', 'M Bags/Pouches', 'MSI', 'Rolls', 'Other']
                    },
                    {
                        'name': 'rollOd',
                        'type': 'numeric',
                        'label': 'Roll OD (in)'
                    }
                ]
            },
            {
                'name': 'BoxPouchRequiredFieldSetName',
                'fields': [
                    {
                        'name': 'width',
                        'type': 'numeric-with-unit',
                        'label': 'Width (W)',
                        'units': ['inches', 'mm']
                    },
                    {
                        'name': 'sideSealWidth',
                        'type': 'numeric',
                        'label': 'Side Seal Width (S)'
                    },
                    {
                        'name': 'height',
                        'type': 'numeric',
                        'label': 'Height (H)'
                    },
                    {
                        'name': 'depth',
                        'type': 'numeric',
                        'label': 'Depth (D)'
                    }
                ]
            },
            {
                'name': 'BoxPouchOptionalFieldSetName',
                'fields': [
                    {
                        'name': 'tear',
                        'type': 'choice-single',
                        'label': 'Tear',
                        'options': ['No notch', 'Tear Notch', 'Slit']
                    },
                    {
                        'name': 'laserScore',
                        'type': 'choice-single',
                        'label': 'Laser Score',
                        'options': ['Yes - 1D', 'Yes - 2D', 'No']
                    },
                    {
                        'name': 'zipper',
                        'type': 'choice-single',
                        'label': 'Zipper',
                        'options': ['None', 'Top Slider', 'Press to Close']
                    },
                    {
                        'name': 'hangHole',
                        'type': 'choice-single',
                        'label': 'Hang Hole',
                        'options': ['Yes', 'No']
                    }
                ]
            },
            {
                'name': 'BoxPouchTerminatedSideGussetRequiredFieldSetName',
                'fields': [
                    {
                        'name': 'width',
                        'type': 'numeric-with-units',
                        'label': 'Width (W)',
                        'units': ['inches', 'mm']
                    },
                    {
                        'name': 'sideSealWidth',
                        'type': 'numeric',
                        'label': 'Side Seal Width (S)'
                    },
                    {
                        'name': 'height',
                        'type': 'numeric',
                        'label': 'Height (H)'
                    },
                    {
                        'name': 'terminatedLength',
                        'type': 'numeric',
                        'label': 'Terminated Length (T)'
                    },
                    {
                        'name': 'depth',
                        'type': 'numeric',
                        'label': 'Depth (D)'
                    }
                ]
            },
            {
                'name': 'BoxPouchTerminatedSideGussetOptionalFieldSetName',
                'fields': [
                    {
                        'name': 'tear',
                        'type': 'choice-single',
                        'label': 'Tear',
                        'options': ['No notch', 'Tear Notch', 'Slit']
                    },
                    {
                        'name': 'laserScore',
                        'type': 'choice-single',
                        'label': 'Laser Score',
                        'options': ['Yes - 1D', 'Yes - 2D', 'No']
                    },
                    {
                        'name': 'zipper',
                        'type': 'choice-single',
                        'label': 'Zipper',
                        'options': ['None', 'Top Slider', 'Press to Close']
                    },
                    {
                        'name': 'hangHole',
                        'type': 'choice-single',
                        'label': 'Hang Hole',
                        'options': ['Yes', 'No']
                    }
                ]
            },
            {
                'name': 'FinSealRequiredFieldSetName',
                'fields': []
            },
            {
                'name': 'FinSealOptionalFieldSetName',
                'fields': []
            }
        ];

		component.set("v.tcProducts", tcProducts);
        component.set("v.tcProductsFieldSets", tcProductsFieldSets);

		var inputTCProducts = component.find("inputTCProducts");
		var opts=[];
		opts.push({"class": "optionClass", label: '--- Please select a product ---', value: null});
		for (var i = 0; i < tcProducts.length; i++) {
			opts.push({"class": "optionClass", label: tcProducts[i].name, value: tcProducts[i].name});
		}
		inputTCProducts.set("v.options", opts);		
	},

	doTCProductSelect : function(component, event) {
		var tcProducts = component.get("v.tcProducts");
        var tcProductsFieldSets = component.get("v.tcProductsFieldSets");

		var tcProductName = event.getSource().get("v.value");

		var tcSelectedProduct;
        var tcSelectedProductRequiredFields;
        var tcSelectedProductOptionalFields;
		for (var i = 0; i < tcProducts.length; i++) {
			if (tcProducts[i].name == tcProductName) {
                tcSelectedProduct = tcProducts[i];

                for (var j = 0; j < tcProductsFieldSets.length; j++) {
                    if (tcProductsFieldSets[j].name == tcSelectedProduct.requiredFieldSet) {
                        tcSelectedProductRequiredFields = tcProductsFieldSets[j];
                    } else if (tcProductsFieldSets[j].name == tcSelectedProduct.optionalFieldSet) {
                        tcSelectedProductOptionalFields = tcProductsFieldSets[j];
                    }
                }
                break;
			}
		}

        component.set("v.tcSelectedProduct", tcSelectedProduct);
        component.set("v.tcSelectedProductRequiredFields", tcSelectedProductRequiredFields);
        component.set("v.tcSelectedProductOptionalFields", tcSelectedProductOptionalFields);

        this.generateFormFieldsWrapper(component, tcSelectedProductRequiredFields.fields, true, "productFormDynamicFieldsRequired");
        this.generateFormFieldsWrapper(component, tcSelectedProductOptionalFields.fields, false, "productFormDynamicFieldsOptional");
	},

    generateFormFieldsWrapper: function(component, fields, required, wrapperId) {
        var wrapper = component.find(wrapperId);
        wrapper.set("v.body", []);

        var globalComponents = [];

        // Callback method to support the following structure
        /*
         * <div aura:id="wrapper">
         *     <input field(s) />
         *     ...
         * </div>
         */
        var createComponentCallback = function (components, status, errorMessage) {
            if (status === "SUCCESS") {
                var wrapperComponent = null;
                for (var i = 0; i < components.length; i++) {
                    if (wrapperComponent == null) {
                        wrapperComponent = components[i];
                        var body = wrapper.get("v.body");
                        body.push(wrapperComponent);
                        wrapper.set("v.body", body);
                    } else {
                        var parentBody = wrapperComponent.get("v.body");
                        parentBody.push(components[i]);
                        wrapperComponent.set("v.body", parentBody);

                        globalComponents.push({value: components[i]});
                        if (required) {
                            component.set("v.requiredFieldComponents", globalComponents);
                        } else {
                            component.set("v.optionalFieldComponents", globalComponents);
                        }
                    }
                }
            } else if (status === "INCOMPLETE") {
                console.error("No response from server or client is offline.")
            } else if (status === "ERROR") {
                console.error("Error: " + errorMessage);
            }
        };

        // Callback method to support the following structure
        /*
         * <div aura:id="wrapper">
         *     <label>
         *         <ui:outputText />
         *     <label>
         *     <input field(s) />
         *     ...
         * </div>
         */
        var createComponentsWithLabelCallback = function (components, status, errorMessage) {
            if (status === "SUCCESS") {
                var wrapperComponent = null;
                var labelComponent = null;
                var isLabelComponentTextSet = false;
                for (var i = 0; i < components.length; i++) {
                    if (wrapperComponent == null) {
                        wrapperComponent = components[i];
                        var body = wrapper.get("v.body");
                        body.push(wrapperComponent);
                        wrapper.set("v.body", body);
                    } else if (labelComponent == null) {
                        labelComponent = components[i];
                        var body = wrapperComponent.get("v.body");
                        body.push(labelComponent);
                        wrapperComponent.set("v.body", body);
                    } else if (!isLabelComponentTextSet) {
                        var labelBody = labelComponent.get("v.body");
                        labelBody.push(components[i]);
                        labelComponent.set("v.body", labelBody);
                        isLabelComponentTextSet = true;
                    } else {
                        var parentBody = wrapperComponent.get("v.body");
                        parentBody.push(components[i]);
                        wrapperComponent.set("v.body", parentBody);

                        globalComponents.push({value: components[i]});
                        if (required) {
                            component.set("v.requiredFieldComponents", globalComponents);
                        } else {
                            component.set("v.optionalFieldComponents", globalComponents);
                        }
                    }
                }
            } else if (status === "INCOMPLETE") {
                console.error("No response from server or client is offline.")
            } else if (status === "ERROR") {
                console.error("Error: " + errorMessage);
            }
        };

        // Callback method to support the following structure
        /*
         * <div aura:id="wrapper">
         *     <input field /> (only one)
         *     <select>
         *         <option />
         *         ...
         *     </select>
         * </div>
         */
        var createNestedComponentsCallback = function (components, status, errorMessage) {
            if (status === "SUCCESS") {
                var wrapperComponent = null;
                var firstNestedComponent = null;
                var parentComponent = null;
                for (var i = 0; i < components.length; i++) {
                    if (wrapperComponent == null) {
                        wrapperComponent = components[i];
                        var body = wrapper.get("v.body");
                        body.push(wrapperComponent);
                        wrapper.set("v.body", body);
                    } else if (firstNestedComponent == null) {
                        firstNestedComponent = components[i];
                        var body = wrapperComponent.get("v.body");
                        body.push(firstNestedComponent);
                        wrapperComponent.set("v.body", body);

                        globalComponents.push({value: components[i]});
                        if (required) {
                            component.set("v.requiredFieldComponents", globalComponents);
                        } else {
                            component.set("v.optionalFieldComponents", globalComponents);
                        }
                    } else if (parentComponent == null) {
                        parentComponent = components[i];
                        var body = wrapperComponent.get("v.body");
                        body.push(parentComponent);
                        wrapperComponent.set("v.body", body);

                        globalComponents.push({value: components[i]});
                        if (required) {
                            component.set("v.requiredFieldComponents", globalComponents);
                        } else {
                            component.set("v.optionalFieldComponents", globalComponents);
                        }
                    } else {
                        var parentBody = parentComponent.get("v.body");
                        parentBody.push(components[i]);
                        parentComponent.set("v.body", parentBody);
                    }
                }
            } else if (status === "INCOMPLETE") {
                console.error("No response from server or client is offline.")
            } else if (status === "ERROR") {
                console.error("Error!");
                console.error(errorMessage);
            }
        };

        // Callback method to support the following structure
        /*
         * <div aura:id="wrapper">
         *     <label>
         *         <ui:outputText />
         *     </label>
         *     <select>
         *         <option />
         *         ...
         *     </select>
         * </div>
         */
        var createNestedComponentsWithLabelCallback = function (components, status, errorMessage) {
            if (status === "SUCCESS") {
                var wrapperComponent = null;
                var labelComponent = null;
                var isLabelComponentTextSet = false;
                var parentComponent = null;
                for (var i = 0; i < components.length; i++) {
                    if (wrapperComponent == null) {
                        wrapperComponent = components[i];
                        var body = wrapper.get("v.body");
                        body.push(wrapperComponent);
                        wrapper.set("v.body", body);
                    } else if (labelComponent == null) {
                        labelComponent = components[i];
                        var body = wrapperComponent.get("v.body");
                        body.push(labelComponent);
                        wrapperComponent.set("v.body", body);
                    } else if (!isLabelComponentTextSet) {
                        var labelBody = labelComponent.get("v.body");
                        labelBody.push(components[i]);
                        labelComponent.set("v.body", labelBody);
                        isLabelComponentTextSet = true;
                    } else if (parentComponent == null) {
                        parentComponent = components[i];
                        var body = wrapperComponent.get("v.body");
                        body.push(parentComponent);
                        wrapperComponent.set("v.body", body);

                        globalComponents.push({value: components[i]});
                        if (required) {
                            component.set("v.requiredFieldComponents", globalComponents);
                        } else {
                            component.set("v.optionalFieldComponents", globalComponents);
                        }
                    } else {
                        var parentBody = parentComponent.get("v.body");
                        parentBody.push(components[i]);
                        parentComponent.set("v.body", parentBody);
                    }
                }
            } else if (status === "INCOMPLETE") {
                console.error("No response from server or client is offline.")
            } else if (status === "ERROR") {
                console.error("Error!");
                console.error(errorMessage);
            }
        };

        for (var i = 0; i < fields.length; i++) {
            var field = fields[i];

            // Wrapper attributes
            var wrapperOpts = {
                "class": "slds-form-element",
                "aura:id": fields[i].name + "-wrapper"
            }

            // Basic component attributes
            var componentOpts = {
                "aura:id": field.name,
                "label": field.label,
                "class": "slds-input",
                "labelClass": "slds-form-element__label"
            };
            if (required) {
                componentOpts.required = "true";
            }

            if (field.type == 'text') {
                $A.createComponents([
                    ["div", wrapperOpts],
                    ["ui:inputText", componentOpts]
                ], createComponentCallback);
            } else if (field.type == 'numeric') {
                $A.createComponents([
                    ["div", wrapperOpts],
                    ["ui:inputNumber", componentOpts]
                ], createComponentCallback);
            } else if (field.type == 'numeric-with-unit') {
                var componentFields = [];
                componentFields.push(["div", wrapperOpts]);
                componentFields.push(["ui:inputNumber", componentOpts]);

                // Add picklist for units
                if (field.units.length > 0) {
                    var componentUnitOpts = {
                        "aura:id": field.name + "-unit",
                        "name": field.name + "-unit",
                        "label": "Unit",
                        "type": "String",
                        "variant": "label-hidden"
                    };

                    componentFields.push(["lightning:select", componentUnitOpts]);
                    for (var j = 0; j < field.units.length; j++) {
                        componentFields.push(["option", {
                            "value": field.units[j],
                            "label": field.units[j]
                        }]);
                    }

                    $A.createComponents(componentFields, createNestedComponentsCallback);
                }
            } else if (field.type == 'choice-single') {
                if (field.options.length > 0) {
                    var componentFields = [];

                    // Wrapper
                    componentFields.push(["div", wrapperOpts]);

                    // Label
                    componentFields.push(
                        ["label", {
                            "class": "slds-form-element__label",
                            "for": field.name,
                            "aura:id": field.name + "-label"
                        }],
                        ["ui:outputText", {
                            "value": field.label
                        }]
                    );

                    // Picklist
                    componentFields.push(["lightning:select", {
                        "aura:id": field.name + "-option",
                        "name": field.name + "-option",
                        "label": "Option",
                        "type": "String",
                        "variant": "label-hidden"
                    }]);
                    if (!required) {
                        componentFields.push(["option", {
                            "value": "",
                            "label": ""
                        }]);

                    }
                    for (var j = 0; j < field.options.length; j++) {
                        componentFields.push(["option", {
                            "value": field.options[j],
                            "label": field.options[j]
                        }]);
                    }

                    $A.createComponents(componentFields, createNestedComponentsWithLabelCallback);
                }
            } else if (field.type == 'choice-multiple') {
                if (field.options.length > 0) {
                    var componentFields = [];

                    // Wrapper
                    componentFields.push(["div", wrapperOpts]);

                    // Field Label
                    componentFields.push(
                        ["label", {
                            "class": "slds-form-element__label",
                            "for": field.name,
                            "aura:id": field.name + "-label"
                        }],
                        ["ui:outputText", {
                            "value": field.label
                        }]
                    );

                    // Checkboxes
                    for (var j = 0; j < field.options.length; j++) {
                        var componentOptionOpts = {
                            "aura:id": field.name + "-option-" + j,
                            "name": field.name,
                            "label": field.options[j],
                            "click": component.getReference("c.checkOption")
                        };
                        if (required) {
                            componentOptionOpts.required = "true";
                        }

                        componentFields.push(["ui:inputCheckbox", componentOptionOpts]);

                        // Special case for "other"
                        if (field.options[j] == "Other") {
                            componentFields.push(["ui:inputText", {
                                "aura:id": field.name + "-option-other",
                                "class": "slds-input hidden",
                                "labelClass": "slds-form-element__label"
                            }]);
                        }
                    }

                    $A.createComponents(componentFields, createComponentsWithLabelCallback);
                }
            }
        }
    },

    saveQuote: function(component, event) {
        var selectedProduct = component.get("v.tcSelectedProduct");
        var selectedRequiredFieldSet = component.get("v.tcSelectedProductRequiredFields");
        var selectedOptionalFieldSet = component.get("v.tcSelectedProductOptionalFields");

        if (this.validateForm(component, selectedRequiredFieldSet.fields)) {
            var product = {};
            product.type = selectedProduct.name;

            var requiredFields = this.getFieldValuesWrapper(component, selectedRequiredFieldSet.fields, true);
            var optionalFields = this.getFieldValuesWrapper(component, selectedOptionalFieldSet.fields, false);
            product.fields = requiredFields.concat(optionalFields);

            console.log(product);
        }
    },

    validateForm: function(component, fields) {
        var globalComponents = component.get("v.requiredFieldComponents");
        var isValid = true;

        for (var i = 0; i < globalComponents.length; i++) {
            var auraId = globalComponents[i].value.getLocalId();
            var value = globalComponents[i].value.get("v.value");

            if (auraId !== undefined) {
                for (var j = 0; j < fields.length; j++) {
                    var field = fields[j];
                    if (field.type == 'text' || field.type == 'numeric' || field.type == 'numeric-with-unit') {
                        if (field.name == auraId) {
                            if (value === undefined || value == "") {
                                globalComponents[i].value.set("v.errors", [{message: "Field is required"}]);
                                isValid = false;
                            } else {
                                globalComponents[i].value.set("v.errors", "");
                            }
                        }
                    }
                }
            }
        }

        return isValid;
    },

    getFieldValuesWrapper: function(component, fields, required) {
        var productFields = [];

        // Special variable to store the checkbox fields value
        var multipleChoicesValues = [];

        var globalComponents = null;
        if (required) {
            globalComponents = component.get("v.requiredFieldComponents");
        } else {
            globalComponents = component.get("v.optionalFieldComponents");
        }

        for (var i = 0; i < globalComponents.length; i++) {
//            console.log(globalComponents[i].value.getLocalId())
//            console.log(globalComponents[i].value.get("v.value"));

            var auraId = globalComponents[i].value.getLocalId();
            var value = globalComponents[i].value.get("v.value");
            if (auraId !== undefined) {
                for (var j = 0; j < fields.length; j++) {
                    var field = fields[j];
                    if (field.type == 'text' || field.type == 'numeric') {
                        if (field.name == auraId) {
                            var productField = {};
                            productField.name = auraId;
                            productField.value = value;
                            productFields.push(productField);
                            break;
                        }
                    } else if (field.type == 'numeric-with-unit') {
                        if (field.name == auraId) {
                            var productField = {};
                            productField.name = auraId;
                            productField.value = value;
                            productFields.push(productField);
                        } else if ((field.name + "-unit") == auraId) {
                            var productField = {};
                            productField.name = auraId;
                            productField.value = value;
                            productFields.push(productField);
                        }
                    } else if (field.type == 'choice-single') {
                        if ((field.name + "-option") == auraId) {
                            var productField = {};
                            productField.name = field.name;
                            productField.value = value;
                            productFields.push(productField);
                        }
                    } else if (field.type == 'choice-multiple') {
                        var selectedValue = null;
                        for (var k = 0; k < field.options.length; k++) {
                            if ((field.name + "-option-" + k) == auraId && value) {
                                selectedValue = field.options[k];
                                break;
                            }
                        }

                        if (selectedValue != null) {
                            var productField = multipleChoicesValues[field.name];
                            if (productField == undefined) {
                                productField = {};
                                productField.name = field.name;
                            }
                            var values = productField['values'];
                            if (values == undefined) {
                                values = [selectedValue];
                            } else {
                                values.push(selectedValue);
                            }
                            productField.values = values;
                            multipleChoicesValues[field.name] = productField;
                        }

                        if ((field.name + "-option-other") == auraId) {
                            var productField = {};
                            productField.name = field.name + "-other";
                            productField.value = value;
                            productFields.push(productField);
                        }
                    }
                }
            }
        }

        for (var fieldName in multipleChoicesValues) {
            productFields.push(multipleChoicesValues[fieldName]);
        }

        return productFields;
    }

})