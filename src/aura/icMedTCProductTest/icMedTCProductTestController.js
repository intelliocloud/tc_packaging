({
	doInit : function(component, event, helper) {
		helper.doInit(component, event);
	},

	doTCProductSelect : function(component, event, helper) {
		helper.doTCProductSelect(component, event);
	},

    saveQuote: function(component, event, helper) {
        helper.saveQuote(component, event);
    },

    selectRadioButton: function(component, event) {
        var auraId = event.getSource().getLocalId();
        var value = event.getSource().get("v.label");
        var hiddenFieldId = null;

        if (auraId.indexOf("-unit") > 0) {
            // Unit type
            hiddenFieldId = auraId.substr(0, auraId.indexOf("-unit") + 5);
        } else if (auraId.indexOf("-option") > 0) {
            // Option type
            hiddenFieldId = auraId.substr(0, auraId.indexOf("-option") + 7);
        }

        var found = false;
        var globalComponents = component.get("v.requiredFieldComponents");
        for (var i = 0; i < globalComponents.length && !found; i++) {
            var componentAuraId = globalComponents[i].value.getLocalId();
            if (componentAuraId == hiddenFieldId) {
                found = true;
                globalComponents[i].value.set("v.value", value);
                break;
            }
        }
        globalComponents = component.get("v.optionalFieldComponents");
        for (var i = 0; i < globalComponents.length && !found; i++) {
            var componentAuraId = globalComponents[i].value.getLocalId();
            if (componentAuraId == hiddenFieldId) {
                found = true;
                globalComponents[i].value.set("v.value", value);
                break;
            }
        }
    },

    checkOption: function(component, event) {
        var auraId = event.getSource().getLocalId();
        var value = event.getSource().get("v.label");
        var checked = event.getSource().get("v.value");

        if (value == "Other") {
            var found = false;
            var hiddenFieldId = null;

            if (auraId.indexOf("-unit") > 0) {
                // Unit type
                hiddenFieldId = auraId.substr(0, auraId.indexOf("-unit") + 5) + "-other";
            } else if (auraId.indexOf("-option") > 0) {
                // Option type
                hiddenFieldId = auraId.substr(0, auraId.indexOf("-option") + 7) + "-other";
            }

            var globalComponents = component.get("v.requiredFieldComponents");
            for (var i = 0; i < globalComponents.length && !found; i++) {
                var componentAuraId = globalComponents[i].value.getLocalId();
                if (componentAuraId == hiddenFieldId) {
                    found = true;
                    if (checked) {
                        $A.util.removeClass(globalComponents[i].value, 'hidden');
                    } else {
                        globalComponents[i].value.set("v.value", undefined);
                        $A.util.addClass(globalComponents[i].value, 'hidden');
                    }
                    break;
                }
            }
            globalComponents = component.get("v.optionalFieldComponents");
            for (var i = 0; i < globalComponents.length && !found; i++) {
                var componentAuraId = globalComponents[i].value.getLocalId();
                if (componentAuraId == hiddenFieldId) {
                    found = true;
                    if (checked) {
                        $A.util.removeClass(globalComponents[i].value, 'hidden');
                    } else {
                        globalComponents[i].value.set("v.value", undefined);
                        $A.util.addClass(globalComponents[i].value, 'hidden');
                    }
                    break;
                }
            }
        }
    }

})