({
	toggle: function (cmp, event) {
		event.stopPropagation();
		var spinner = cmp.find("mySpinner");
		$A.util.toggleClass(spinner, "slds-hide");
	}
})