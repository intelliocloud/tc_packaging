({
	doInit : function(component, event, helper) {
		helper.doInit(component, event);
	},

	doTCProductSelect : function(component, event, helper) {
		helper.doTCProductSelect(component, event);
	},

    saveQuote: function(component, event, helper) {
        helper.saveQuote(component, event);
    },

    selectRadio: function(component, event, helper) {
        console.log("radio selected");
    }

})