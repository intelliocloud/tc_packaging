({
	doInit : function(component, event) {
		var tcProducts = [
			{
                'name' : 'Rollstock',
                'requiredFieldSet' : 'RollstockRequiredFieldSetName',
                'optionalFieldSet' : 'RollstockOptionalFieldSetName'
            },
			{
                'name' : 'Pouch - Box Pouch',
                'requiredFieldSet' : 'BoxPouchRequiredFieldSetName',
                'optionalFieldSet' : 'BoxPouchOptionalFieldSetName'
            },
			{
                'name' : 'Pouch - Box Pouch with Terminated Side Gusset',
                'requiredFieldSet' : 'BoxPouchTerminatedSideGussetRequiredFieldSetName',
                'optionalFieldSet' : 'BoxPouchTerminatedSideGussetOptionalFieldSetName'
            },
			{
                'name' : 'Pouch - Fin Seal',
                'requiredFieldSet' : 'FinSealRequiredFieldSetName',
                'optionalFieldSet' : 'FinSealOptionalFieldSetName'
            }
		]

        var tcProductsFieldSets = [
            {
                'name': 'RollstockRequiredFieldSetName',
                'fields': [
                    {
                        'name': 'webWidth',
                        'type': 'numeric-with-unit',
                        'label': 'Finished Web Width',
                        'units': ['inches', 'mm'],
                        'unitsDisplayType': 'radio'
                    },
                    {
                        'name': 'nbImpressionsWeb',
                        'type': 'numeric',
                        'label': '# of Impressions Across Web'
                    },
                    {
                        'name': 'coreSize',
                        'type': 'choice-single',
                        'label': 'Core Size',
                        'options': ['3"', '6"', 'Unknown', 'N/A']
                    },
                    {
                        'name': 'cutoffRepeat',
                        'type': 'numeric-with-unit',
                        'label': 'Cutoff/Repeat',
                        'units': ['inches', 'mm'],
                        'unitsDisplayType': 'radio'
                    },
                    {
                        'name': 'nbImpressionsCutoff',
                        'type': 'numeric',
                        'label': '# of Impressions per Cutoff'
                    }
                ]
            },
            {
                'name': 'RollstockOptionalFieldSetName',
                'fields': [
                    {
                        'name': 'finishingRequirements',
                        'type': 'choice-multiple',
                        'label': 'Finishing Requirements',
                        'options': ['Laser Score', 'Other']
                    },
                    {
                        'name': 'quantityUomRoll',
                        'type': 'numeric-with-unit',
                        'label': 'Quantity & UOM/Roll',
                        'units': ['Imps', 'M Imps', 'Kg', 'Lbs', 'Linear Feet', 'Linear Meters', 'M Bags/Pouches', 'MSI', 'Rolls', 'Other'],
                        'unitsDisplayType': 'picklist'
                    },
                    {
                        'name': 'rollOd',
                        'type': 'numeric',
                        'label': 'Roll OD (in)'
                    }
                ]
            },
            {
                'name': 'BoxPouchRequiredFieldSetName',
                'fields': [
                    {
                        'name': 'width',
                        'type': 'numeric-with-unit',
                        'label': 'Width (W)',
                        'units': ['inches', 'mm'],
                        'unitsDisplayType': 'radio'
                    },
                    {
                        'name': 'sideSealWidth',
                        'type': 'numeric',
                        'label': 'Side Seal Width (S)'
                    },
                    {
                        'name': 'height',
                        'type': 'numeric',
                        'label': 'Height (H)'
                    },
                    {
                        'name': 'depth',
                        'type': 'numeric',
                        'label': 'Depth (D)'
                    }
                ]
            },
            {
                'name': 'BoxPouchOptionalFieldSetName',
                'fields': [
                    {
                        'name': 'tear',
                        'type': 'choice-single',
                        'label': 'Tear',
                        'options': ['No notch', 'Tear Notch', 'Slit']
                    },
                    {
                        'name': 'laserScore',
                        'type': 'choice-single',
                        'label': 'Laser Score',
                        'options': ['Yes - 1D', 'Yes - 2D', 'No']
                    },
                    {
                        'name': 'zipper',
                        'type': 'choice-single',
                        'label': 'Zipper',
                        'options': ['None', 'Top Slider', 'Press to Close']
                    },
                    {
                        'name': 'hangHole',
                        'type': 'choice-single',
                        'label': 'Hang Hole',
                        'options': ['Yes', 'No']
                    }
                ]
            },
            {
                'name': 'BoxPouchTerminatedSideGussetRequiredFieldSetName',
                'fields': []
            },
            {
                'name': 'BoxPouchTerminatedSideGussetOptionalFieldSetName',
                'fields': []
            },
            {
                'name': 'FinSealRequiredFieldSetName',
                'fields': []
            },
            {
                'name': 'FinSealOptionalFieldSetName',
                'fields': []
            }
        ];

		component.set("v.tcProducts", tcProducts);
        component.set("v.tcProductsFieldSets", tcProductsFieldSets);

//        component.set("v.tcProductSelected", tcProducts[0]);
//        component.set("v.tcProductRequiredFieldSetSelected", tcProductsFieldSets['RollstockRequiredFieldSetName']);
//        component.set("v.tcProductOptionalFieldSetSelected", tcProductsFieldSets['RollstockOptionalFieldSetName']);

		var inputTCProducts = component.find("inputTCProducts");
		var opts=[];
		opts.push({"class": "optionClass", label: '--- Please select a product ---', value: null});
		for (var i = 0; i < tcProducts.length; i++) {
			opts.push({"class": "optionClass", label: tcProducts[i].name, value: tcProducts[i].name});
		}
		inputTCProducts.set("v.options", opts);		
	},

	doTCProductSelect : function(component, event) {
		var tcProducts = component.get("v.tcProducts");
        var tcProductsFieldSets = component.get("v.tcProductsFieldSets");

		var tcProductName = event.getSource().get("v.value");

		var tcSelectedProduct;
        var tcSelectedProductRequiredFields;
        var tcSelectedProductOptionalFields;
		for (var i = 0; i < tcProducts.length; i++) {
			if (tcProducts[i].name == tcProductName) {
                tcSelectedProduct = tcProducts[i];

                for (var j = 0; j < tcProductsFieldSets.length; j++) {
                    if (tcProductsFieldSets[j].name == tcSelectedProduct.requiredFieldSet) {
                        tcSelectedProductRequiredFields = tcProductsFieldSets[j];
                    } else if (tcProductsFieldSets[j].name == tcSelectedProduct.optionalFieldSet) {
                        tcSelectedProductOptionalFields = tcProductsFieldSets[j];
                    }
                }
                break;
			}
		}

        component.set("v.tcSelectedProduct", tcSelectedProduct);
        component.set("v.tcSelectedProductRequiredFields", tcSelectedProductRequiredFields);
        component.set("v.tcSelectedProductOptionalFields", tcSelectedProductOptionalFields);

        this.generateFormFields(component, tcSelectedProductRequiredFields, true, "productFormDynamicFieldsRequired");
        this.generateFormFields(component, tcSelectedProductOptionalFields, false, "productFormDynamicFieldsOptional");
	},

    generateFormFields: function(component, fields, required, wrapperId) {
        var wrapper = component.find(wrapperId);
        wrapper.set("v.body", []);

        var createComponentCallback = function (component, status, errorMessage) {
            if (status === "SUCCESS") {
                var body = wrapper.get("v.body");
                body.push(component);
                wrapper.set("v.body", body);
            } else if (status === "INCOMPLETE") {
                console.error("No response from server or client is offline.")
            } else if (status === "ERROR") {
                console.error("Error: " + errorMessage);
            }
        };
        var createNestedComponentsCallback = function (components, status, errorMessage) {
            if (status === "SUCCESS") {
                var parentComponent = null;
                for (var i = 0; i < components.length; i++) {
                    if (parentComponent == null) {
                        parentComponent = components[i];
                        var body = wrapper.get("v.body");
                        body.push(parentComponent);
                        wrapper.set("v.body", body);
                    } else {
                        var parentBody = parentComponent.get("v.body");
                        parentBody.push(components[i]);
                        parentComponent.set("v.body", parentBody);
                    }
                }
            } else if (status === "INCOMPLETE") {
                console.error("No response from server or client is offline.")
            } else if (status === "ERROR") {
                console.error("Error!");
                console.error(errorMessage);
            }
        };

        for (var i = 0; i < fields.fields.length; i++) {
            var field = fields.fields[i];
            var componentOpts = {
                "aura:id": field.name,
                "label": field.label,
                "class": "slds-input",
                "labelClass": "slds-form-element__label"
            };
            if (required) {
                componentOpts.required = "true";
            }

            if (field.type == 'text') {
                $A.createComponent(
                    "ui:inputText",
                    componentOpts,
                    createComponentCallback
                );
            } else if (field.type == 'numeric') {
                $A.createComponent(
                    "ui:inputNumber",
                    componentOpts,
                    createComponentCallback
                );
            } else if (field.type == 'numeric-with-unit') {
                $A.createComponent(
                    "ui:inputNumber",
                    componentOpts,
                    createComponentCallback
                );

                if (field.units.length > 0) {
                    if (field.unitsDisplayType == "radio") {
                        for (var j = 0; j < field.units.length; j++) {
                            var componentUnitOpts = {
                                "aura:id": field.name + "-unit-" + j,
                                "name": field.name + "-unit",
                                "label": field.units[j],
                                "text": field.units[j],
                                "onclick": component.getReference("c.selectRadio")
                            };

                            $A.createComponent(
                                "ui:inputRadio",
                                componentUnitOpts,
                                createComponentCallback
                            )
                        }
                    } else if (field.unitsDisplayType == "picklist") {
                        var componentUnitOpts = {
                            "aura:id": field.name + "-unit",
                            "name": field.name + "-unit",
                            "label": "Unit",
                            "type": "String",
                            "variant": "label-hidden"
                        };
                        if (required) {
                            componentUnitOpts.required = "true";
                        }

                        var unitFields = [];
                        unitFields.push(["lightning:select", componentUnitOpts]);
                        for (var j = 0; j < field.units.length; j++) {
                            unitFields.push(["option", {
                                "value": field.units[j],
                                "label": field.units[j]
                            }]);
                        }

                        $A.createComponents(unitFields, createNestedComponentsCallback);
                    }
                }
            } else if (field.type == 'choice-single') {
                if (field.options.length > 0) {
                    $A.createComponents([
                        ["label", {
                            "class": "-form-element__label",
                            "for": field.name
                        }],
                        ["ui:outputText", {
                            "value": field.label
                        }]
                    ], createNestedComponentsCallback);

                    for (var j = 0; j < field.options.length; j++) {
                        var componentOptionOpts = {
                            "aura:id": field.name + "-option-" + j,
                            "name": field.name,
                            "label": field.options[j],
                            "onclick": component.getReference("c.selectRadio")
                        };
                        if (required) {
                            componentOptionOpts.required = "true";
                        }

                        $A.createComponent(
                            "ui:inputRadio",
                            componentOptionOpts,
                            createComponentCallback
                        )
                    }
                }
            } else if (field.type == 'choice-multiple') {
                if (field.options.length > 0) {
                    $A.createComponents([
                        ["label", {
                            "class": "-form-element__label",
                            "for": field.name
                        }],
                        ["ui:outputText", {
                            "value": field.label
                        }]
                    ], createNestedComponentsCallback);

                    for (var j = 0; j < field.options.length; j++) {
                        var componentOptionOpts = {
                            "aura:id": field.name + "-option-" + j,
                            "name": field.name,
                            "label": field.options[j]
                        };
                        if (required) {
                            componentOptionOpts.required = "true";
                        }

                        $A.createComponent(
                            "ui:inputCheckbox",
                            componentOptionOpts,
                            createComponentCallback
                        )
                    }
                }
            }
        }
    },

    saveQuote: function(component, event) {
        var selectedProduct = component.get("v.tcSelectedProduct");
        var selectedRequiredFieldSet = component.get("v.tcSelectedProductRequiredFields");
        var selectedOptionalFieldSet = component.get("v.tcSelectedProductOptionalFields");

        // TODO Validate form (required fields)
        // *** This approach does not seem possible *** //
        // https://salesforce.stackexchange.com/questions/154416/lightning-components-can-auraid-be-set-to-an-component-attribute

        var product = {};
        product.type = selectedProduct.name;

        var requiredFields = this.getFieldValues(component, selectedRequiredFieldSet.fields, "productFormDynamicFieldsRequired");
        var optionalFields = this.getFieldValues(component, selectedOptionalFieldSet.fields, "productFormDynamicFieldsOptional");
        product.fields = requiredFields.concat(optionalFields);

        console.log(product);
    },

    getFieldValues: function(component, fields, wrapperId) {
        var wrapper = component.find(wrapperId);
        var body = wrapper.get("v.body");
        var productFields = [];

        // Special variable to store the checkbox fields value
        var multipleChoicesValues = [];

        for (var i = 0; i < body.length; i++) {
            var auraId = body[i].getLocalId();
            if (auraId !== undefined) {
                var value = body[i].get("v.value");

                for (var j = 0; j < fields.length; j++) {
                    var field = fields[j];
                    if (field.type == 'text' || field.type == 'numeric') {
                        if (field.name == auraId) {
                            var productField = {};
                            productField.name = auraId;
                            productField.value = value;
                            productFields.push(productField);
                            break;
                        }
                    } else if (field.type == 'numeric-with-unit') {
                        if (field.name == auraId) {
                            var productField = {};
                            productField.name = auraId;
                            productField.value = value;
                            productFields.push(productField);
                        } else if (field.unitsDisplayType == 'radio') {
                            for (var k = 0; k < field.units.length; k++) {
                                if ((field.name + "-unit-" + k) == auraId && value) {
                                    var productField = {};
                                    productField.name = field.name + "-unit";
                                    productField.value = field.units[k];
                                    productFields.push(productField);
                                    break;
                                }
                            }
                        } else if (field.unitsDisplayType == "picklist") {
                            if ((field.name + "-unit") == auraId) {
                                var productField = {};
                                productField.name = auraId;
                                productField.value = value;
                                productFields.push(productField);
                            }
                        }
                    } else if (field.type == 'choice-single') {
                        for (var k = 0; k < field.options.length; k++) {
                            if ((field.name + "-option-" + k) == auraId && value) {
                                var productField = {};
                                productField.name = field.name;
                                productField.value = field.options[k];
                                productFields.push(productField);
                                break;
                            }
                        }
                    } else if (field.type == 'choice-multiple') {
                        var selectedValue = null;
                        for (var k = 0; k < field.options.length; k++) {
                            if ((field.name + "-option-" + k) == auraId && value) {
                                selectedValue = field.options[k];
                                break;
                            }
                        }

                        if (selectedValue != null) {
                            var productField = multipleChoicesValues[field.name];
                            if (productField == undefined) {
                                productField = {};
                                productField.name = field.name;
                            }
                            var values = productField['values'];
                            if (values == undefined) {
                                values = [selectedValue];
                            } else {
                                values.push(selectedValue);
                            }
                            productField.values = values;
                            multipleChoicesValues[field.name] = productField;
                        }
                    }
                }
            }
        }

        for (var fieldName in multipleChoicesValues) {
            productFields.push(multipleChoicesValues[fieldName]);
        }

        return productFields;
    }
})