({
	showComponent : function(component) {
		$A.util.removeClass(component.find("animated"), "flipOutX");
		component.set("v.show", true);
		var delay = component.get("v.delay");
		var helper = this;
		if (delay) {
			window.setTimeout(function() {
				helper.closeComponent(component);
			}, delay * 1000);
		}
	},

	closeComponent : function(component) {
		$A.util.addClass(component.find("animated"),"flipOutX");
		window.setTimeout(function() {
				component.set("v.show",false);
		},1000);
	},

	initComponent : function(component,event) {

		component.set("v.title", event.getParam("title"));
		component.set("v.type", event.getParam("type"));
		component.set("v.message", event.getParam("message"));
		component.set("v.delay", event.getParam("delay"));

		var type = component.get("v.type");

		if(type.toLowerCase() == "success" || type.toLowerCase() == "error") {
			component.set("v.hasIcon",true);
			if(type.toLowerCase() == "error") component.set("v.iconName","warning");
		}

		if(type.toLowerCase() == "warning") {
			component.set("v.btnIconClass","slds-button slds-notify__close");
		}
	}
})