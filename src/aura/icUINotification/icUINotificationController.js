({
	close: function(component, event, helper) {
		helper.closeComponent(component);
	},

	show: function(component, event, helper) {
		helper.initComponent(component,event);
		helper.showComponent(component,event);
	}
})