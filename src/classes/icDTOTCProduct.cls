global with sharing class icDTOTCProduct {
	@AuraEnabled global String name {get;set;}
	@AuraEnabled global String requiredFieldSet {get;set;}
	@AuraEnabled global String optionalFieldSet {get;set;}
}