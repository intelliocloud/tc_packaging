global with sharing class icCTRLTCProduct {
	//static icBusinessLogicTCProduct.IClass blTCProduct = (icBusinessLogicTCProduct.IClass) icObjectFactory.getSingletonInstance('icBusinessLogicTCProduct');

	@AuraEnabled
	global static List<icDTOTCProduct> getTCProductsForPlant(String plant) {
		List<icDTOTCProduct> returnList = new List<icDTOTCProduct>();

		returnList.add(new icDTOTCProduct());

		return returnList;
	}
}