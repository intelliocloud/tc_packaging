/**
 * Created by leungh on 2017-08-29.
 */

global with sharing class icDTOTCProductField {
    @AuraEnabled global String name {get;set;}
    @AuraEnabled global String type {get;set;}
    @AuraEnabled global String label {get;set;}
    @AuraEnabled global String options {get;set;}
    @AuraEnabled global String units {get;set;}
    @AuraEnabled global String unitsDisplayType {get;set;}
}